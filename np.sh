#!/bin/bash


created_at=`date "+%Y-%m-%d %H:%m:%S %z"`
name_date=`date "+%Y-%m-%d"`
title="apost"
if [ x$1 != x ];then
    echo "title: "$1
    title=$1
else
    echo "Need title"
    exit 0
fi

content="---\nlayout: post\ndate: "$created_at"\ntitle: "$title"\n---\n\n\n<!--more-->"

filename=$name_date"-"$title".markdown"
echo $filename
path="_posts/"$filename
if [ ! -f "$path" ];then
    echo "ok"
    touch $path
    echo $content > $path
    cat $path
else
    echo "error"
fi
